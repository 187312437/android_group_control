﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adb群控.model
{
    public class Device
    {

        /// <summary>
        /// 设备名称
        /// </summary>
        private String name;

        /// <summary>
        /// 设备宽
        /// </summary>
        private String width;

        /// <summary>
        /// 设备高
        /// </summary>
        private String higth;

        /// <summary>
        /// cup型号
        /// </summary>
        private String cupAbi;

        /// <summary>
        /// sdk版本信息
        /// </summary>
        private String sdkVersion;

        /// <summary>
        /// 状态
        /// </summary>
        private String status;



        /// <summary>
        /// 设备端口
        /// </summary>
        private int port;

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public String Width
        {
            get
            {
                return width;
            }

            set
            {
                width = value;
            }
        }

        public String Higth
        {
            get
            {
                return higth;
            }

            set
            {
                higth = value;
            }
        }

        public string CupAbi
        {
            get
            {
                return cupAbi;
            }

            set
            {
                cupAbi = value;
            }
        }

        public string SdkVersion
        {
            get
            {
                return sdkVersion;
            }

            set
            {
                sdkVersion = value;
            }
        }

        public int Port
        {
            get
            {
                return port;
            }

            set
            {
                port = value;
            }
        }

        public string Status
        {
            get
            {
                return status;
            }

            set
            {
                status = value;
            }
        }
    }
}
